"""
Day 2-1 Score = Hand + Outcome
"""

with open("2022_02_example.txt", "r", encoding="utf-8") as file:
    example = file.readlines()
    # ['A Y\n', 'B X\n', 'C Z\n']

clean_input = (",".join([x.strip() for x in example])).split(",")
# ['A Y', 'B X', 'C Z']

moves_list = [moves.split(" ") for moves in clean_input]
# [['A', 'Y'], ['B', 'X'], ['C', 'Z']]

# define winning and losing moves
lose_moves = {
    "A" : "Z", # Rock > Scissors
    "B" : "X", # Paper > Rock
    "C" : "Y"  # Scissors > Paper
}

win_moves = {
    "A" : "Y", # Rock < Paper
    "B" : "Z", # Paper < Scissors
    "C" : "X"  # Scissors < Rock
}

# define move scores
move_scores = {
    "X" : 1, # Rock = 1
    "Y" : 2, # Paper = 2
    "Z" : 3  # Scissors = 3
}

# for i, move in enumerate(moves_list):
#     print(i, move)


score = 0
for i, moves in enumerate(moves_list):
    # print(i, moves)
    op_move = moves[0]
    my_move = moves[1]

    # if ops move and my move in win combos
    if (op_move in win_moves) and (my_move in win_moves[op_move]):
        score += (move_scores[my_move] + 6)
        # print(moves_list[moves], "win")

    # if ops move and my move in lose combos
    elif (op_move in lose_moves) and (my_move in lose_moves[op_move]):
        score += (move_scores[my_move])
        # print(moves_list[moves], "lose")

    else:
        score += (move_scores[my_move]) + 3
        # print(moves_list[moves], "tie")

print(score)


"""
Part Two: My move changes according to opponent move
"""

part_two_moves = {
    "X" : 0, # lose
    "Y" : 3, # draw
    "Z" : 6  # win
}

score_2 = 0
for i, moves in enumerate(moves_list):
    # print(i, moves)
    op_move = moves[0]
    my_move = moves[1]   

    if op_move == "A": # Rock
        if my_move == "X": # lose (Scissor)
            score_2 += (part_two_moves[my_move] + move_scores["Z"])

        elif my_move == "Y": # draw (Rock)
            score_2 += (part_two_moves[my_move] + move_scores["X"])

        else: # win (Paper)
            score_2 += (part_two_moves[my_move] + move_scores["Y"])

    elif op_move == "B": # Paper
        if my_move == "X": # lose (Rock)
            score_2 += (part_two_moves[my_move] + move_scores["X"])

        elif my_move == "Y": # draw (Paper)
            score_2 += (part_two_moves[my_move] + move_scores["Y"])

        else: # win (Scissor)
            score_2 += (part_two_moves[my_move] + move_scores["Z"])

    elif op_move == "C": # Scissor
        if my_move == "X": # lose (Paper)
            score_2 += (part_two_moves[my_move] + move_scores["Y"])

        elif my_move == "Y": # draw (Scissor)
            score_2 += (part_two_moves[my_move] + move_scores["Z"])

        else: # win (Rock)
            score_2 += (part_two_moves[my_move] + move_scores["X"])

print(score_2)

