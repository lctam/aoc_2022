"""
Day 4-1 Finding whether one elf's numbers is a subset of another elf's

Day 4-2 Finding number of intersects between pairs of elves
"""

with open("2022_04.txt", "r") as file:
    example = file.read().strip()

input = example.split("\n")
input2 = [item.split(",") for item in input]
input3 = [[item.split("-") for item in lst]
                for lst in input2]

# turning strings from input3 into ranges
def str_to_ranges(lst):
    if lst[0] != lst[1]:
        return range(int(lst[0]), int(lst[1]) + 1)

    return int(lst[0])

# turning ranges into list of numbers
def range_to_numbers(ranges):
    if not isinstance(ranges, int):
        return list(ranges)

    return [ranges]


ranges_1, ranges_2 = [], []
for lst_1, lst_2 in input3:
    ranges_1.append(str_to_ranges(lst_1))
    ranges_2.append(str_to_ranges(lst_2))

numbers_1, numbers_2 = [], []
for ranges in ranges_1:
    numbers_1.append(range_to_numbers(ranges))

for ranges in ranges_2:
    numbers_2.append(range_to_numbers(ranges))

subsets, intersects = 0, 0
for n, numbers in enumerate(numbers_1):
    # print(numbers_1[n], numbers_2[n])
    if set(numbers_1[n]).issubset(set(numbers_2[n])):
        subsets += 1
        intersects += 1

    elif set(numbers_2[n]).issubset(set(numbers_1[n])):
        subsets += 1
        intersects += 1

    elif set(numbers_1[n]).intersection(set(numbers_2[n])):
        intersects += 1

    elif set(numbers_2[n]).intersection(set(numbers_1[n])):
        intersects += 1

print(subsets)    # part one
print(intersects) # part two

