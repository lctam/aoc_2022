"""
Day3-1 Find priority score from common letter in two halves of each line
"""

import string

with open("2022_03.txt", "r") as file:
    example = file.readlines()

clean_input = (",".join([x.strip() for x in example])).split(",")

lowercase = list(string.ascii_lowercase)
uppercase = list(string.ascii_uppercase)

priority = 0
for n, item in enumerate(clean_input):

    first_half = item[slice(0, len(item)//2)]
    second_half = item[slice(len(item)//2, len(item))]
    (common_letter,) = set(first_half).intersection(second_half) # unpacking tuple
    # print(common_letter)

    if common_letter.islower():
        priority += lowercase.index(common_letter) + 1

    if common_letter.isupper():
        priority += uppercase.index(common_letter) + 27

print(priority)

"""
Day3-2 find common letter in groups of three lines
"""

triplets = [clean_input[item:item + 3]
                for item in range(0, len(clean_input), 3)]
priority_2 = 0
for n, item in enumerate(triplets):

    # "*" unpacks items in set(item[1:]) into individual params
    (common_letter,) = set(item[0]).intersection(*set(item[1:]))
    # print(common_letter)

    if common_letter.islower():
        priority_2 += lowercase.index(common_letter) + 1

    if common_letter.isupper():
        priority_2 += uppercase.index(common_letter) + 27

print(priority_2)

