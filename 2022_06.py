"""
Day6-1 Find number of characters before first start-of-packet marker is detected
        ... start-of-packet is the first four unique characters in given string
        ... start-of-packet is first 14 unique character for Day6-2
"""

example = "mjqjpqmgbljsphdztnvjfqwrcgsmlb" #7
example1 = "bvwbjplbgvbhsrlpgdmjqwftvncz"  #5
example2 = "nppdvjthqldpwncqszvftbrmjlhg"  #6
example3 = "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg" #10
example4 = "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw" #11

# slider of four characters to move along string

with open("2022_06.txt") as file:
    signal = file.read()

def check_marker(sequence):
    checker = set(sequence)
    return len(checker), checker

marker_lst = []
marker_length = 4       # part one
# marker_length = 14    # part two

for n, char in enumerate(signal):
    marker = check_marker(signal[n:n + marker_length])
    marker_lst.append(marker)

    if marker[0] == marker_length:
        print(f"where marker is received: {(n+1) + marker_lst[n-1][0]}")
        break
