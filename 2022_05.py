"""
Day5-1: Move crates one at a time

Day5-2: Move few crates in same order
"""
import numpy as np
import re

fp_ex = "2022_05_example.txt"
fp = "2022_05.txt"

with open(fp_ex, "r") as file:
    example = file.readlines()

# finding index of empty line
index = -1
for line in example:
    index += 1
    if len(line) == 1:
        split_at = index

# separating crates and moves
crates = list(example)[:split_at - 1]

# extract every 4th character for letters
crates_split = []
for line in crates:
    for n in range(1, len(line), 4):
        crates_split.append(line[n])

# grouping crates_split
n_grp = int(len(crates[0]) / 4)
grouped_crates = [crates_split[i:i + n_grp]
                    for i in range(0, len(crates_split), n_grp)]
"""
[[' ', 'D', ' '],
 ['N', 'C', ' '],
 ['Z', 'M', 'P']]
"""

matrix = np.array(grouped_crates)
transposed = matrix.transpose() # to put each column into list
trans_matrix = [list(arr) for arr in transposed]

"""
[[' ' 'N' 'Z']
 ['D' 'C' 'M']
 [' ' ' ' 'P']]
"""

# processing moves list
moves = [line.split("\n")[0] for line in example[split_at:]
            if line.split("\n")[0] != ""]
move_n = [re.findall(r'\d+', move) for move in moves]
move_int = [[int(move) for move in lst] for lst in move_n]


def crate_mover(moves, matrix, model):
    crate_n     = moves[0]      # moving 1 crate
    crate_start = moves[1] - 1  # from list 1
    crate_end   = moves[2] - 1  # to list 0

    # idenitify crates from start to move
    for lst in matrix:
        if " " in lst:
            lst.remove(" ")

    if model == 9000: # moving crates one at a time
        for moving_crate in matrix[crate_start][:crate_n]:

            matrix[crate_start].remove(moving_crate)
            head = [moving_crate]
            matrix[crate_end] = head + matrix[crate_end]

    elif model == 9001: # moving crates as a list with same order
        moving_stack = matrix[crate_start][:crate_n]

        for crate in moving_stack:
            matrix[crate_start].remove(crate)

        matrix[crate_end] = moving_stack + matrix[crate_end]

    return matrix


# start moving crates...

# part 1: set model=9000
# part 2: set model=9001
for move in move_int:
    done = crate_mover(move, trans_matrix, model=9000)

print([crate[0] for crate in done if crate[0] != []])

