package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"time"

	"github.com/gocolly/colly"
)

type Credentials struct {
	Username string
	Password string
	Host     string
	Port     string
}

// declaring json struct format
type JsonOutput struct {
	IP       string `json:"ip"`
	CITY     string `json:"city"`
	REGION   string `json:"region`
	COUNTRY  string `json:"country"`
	LOC      string `json:"loc"`
	ORG      string `json:"org"`
	POSTAL   string `json:"postal"`
	TIMEZONE string `json:"timezone"`
	README   string `json:"readme"`
}

// to store unique IPs
var testSlice []string
var badSlice []string

// removes duplicates in slice
func unique[T comparable](s []T) []T {
	inResult := make(map[T]bool)
	var result []T
	for _, str := range s {
		if _, ok := inResult[str]; !ok {
			inResult[str] = true
			result = append(result, str)
		}
	}
	return result
}

// func NodeTest(uid int, country string, wg *sync.WaitGroup) {
func NodeTest(uid int, country string) {

	// defer wg.Done()

	var creds = &Credentials{}
	// var user = fmt.Sprintf("evpn-%d", uid)
	creds.Username = fmt.Sprintf("evpn-%d", uid)
	creds.Password = ""
	// creds.Host = "sgs-rose.salad.com" // UK
	creds.Host = "hostname" // IN
	creds.Port = "1234"

	// Instantiate default collector
	c := colly.NewCollector(
		colly.AllowURLRevisit(),
		colly.Async(true), // Async may be useful for Prometheus push
	)

	// Set Proxy
	var proxy = fmt.Sprintf(
		"https://%s:%s@%s:%s",
		creds.Username,
		creds.Password,
		creds.Host,
		creds.Port,
	)

	c.SetProxy(proxy)
	c.SetRequestTimeout(5 * time.Second)

	// Print the Response
	// * asterisk decalres r as pointer to colly.Response type
	c.OnResponse(func(r *colly.Response) {
		// fmt.Printf("%s", bytes.Replace(r.Body, []byte(""), nil, -1))
		// fmt.Printf("%d\n", r.StatusCode)
		// fmt.Printf("%s\n", r.Body)

		// declaring jsonResponse as JsonOuput Object
		// jsonResponse := JsonOutput{}
		jsonResponse := new(JsonOutput)

		// Unmarshal decodes uint8 bytes into json
		json.Unmarshal(r.Body, &jsonResponse)

		var newIP = jsonResponse.IP
		testSlice = append(testSlice, newIP)

		if jsonResponse.COUNTRY != country {
			badSlice = append(badSlice, newIP)
		}

		fmt.Println(
			jsonResponse.IP,
			jsonResponse.COUNTRY,
			jsonResponse.REGION,
			r.StatusCode,
		)

	})

	// Print Error
	c.OnError(func(r *colly.Response, err error) {
		log.Println(err)
	})

	// Request Page
	c.Visit("https://ipinfo.io/json")
	c.Wait() // Async

}

func main() {

	// var wg sync.WaitGroup

	// start timing
	start := time.Now()

	for i := 0; i < 50; i++ {

		// wg.Add(1)

		rand := rand.Perm(9999)

		if rand[0] < 1000 {
			rand[0] += 1000
		}

		// go NodeTest(rand[0], "IN", &wg)
		NodeTest(rand[0], "IN")

		// wg.Wait()
	}

	fmt.Printf("Unique IPs Count: %d\n", len(unique(testSlice)))
	fmt.Println(badSlice)

	// for n, ip := range unique(testSlice) {
	// 	fmt.Println(n, ip)
	// }

	elapsed := time.Since(start)
	log.Printf("test took %s", elapsed)

}
