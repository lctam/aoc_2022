"""
Day 1-1 Find max calories in list
"""

with open("2022_01_example.txt", "r", encoding="utf-8") as file:
    example = file.read()

calories = []

for cal in example.split("\n\n"):  
    # ['1000\n2000\n3000', '4000', '5000\n6000', '7000\n8000\n9000', '10000']

    item = cal.split("\n")
    item_int = [int(cal) for cal in item]
    calories.append(item_int)

# calories = [[1000, 2000, 3000], [4000], [5000, 6000], [7000, 8000, 9000], [10000]]
sum_cal = [sum(cal) for cal in calories]

print(max(sum_cal))


"""
Day1-2 Find top 3 max and total them
"""

# sorting sums from largest to smallest
sum_cal.sort(reverse=True)
top_three = sum(sum_cal[:3])

print(top_three)
