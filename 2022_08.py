import numpy as np

with open("2022_08_example.txt") as file:
    trees = file.read().splitlines()

# ['30373', '25512', '65332', '33549', '35390']
"""
30373
25512
65332
33549
35390
"""
# get shape
x, y = len(trees[0]), len(trees)
print(x, y)

# perimerter trees are visible so (2x + 2y) - 4
edge_trees = 2*x + 2*y - 4
print(edge_trees)

# init empty matrix for trees
tree_grid = np.zeros((x, y))

for n, tree in enumerate(trees):
    # print(n, tree)
    arr = [int(one) for one in tree]
    np.copyto(tree_grid[n], arr)

"""
[[3. 0. 3. 7. 3.]
 [2. 5. 5. 1. 2.]
 [6. 5. 3. 3. 2.]
 [3. 3. 5. 4. 9.]
 [3. 5. 3. 9. 0.]]
"""

# to store visible tree locations and their counts
visible_trees = np.zeros((x, y))
"""
starting from the middle rows, check each tree:
    - row-wise from left 
    - row-wise from right
    - column-wise from top
    - column-wise from bottom
visibile_tree + 1 if the tree is highest in any one of the four checks

### add each tree into buffer list to keep track of previous trees in row
"""

def check_trees(row_col, index, axis=0):
    # for y, row in enumerate(row_col):
    buffer = []

    if axis == 0:
        for x, tree in enumerate(row_col):
            buffer.append(tree)

            # how to prevent trees with same height and far apart from being counted
            if tree > row_col[x - 1] and tree == max(buffer):
                visible_trees[x, index] = 1

    elif axis == 1:
        for y, tree in enumerate(row_col):
            buffer.append(tree)

            if tree > row_col[y - 1] and tree == max(buffer):
                visible_trees[index, y] = 1



# left > right
for y, row in enumerate(tree_grid):
    check_trees(row, y, axis=0)

# right > left
for y, row in enumerate(tree_grid):
    # row_r = list(reversed(row))
    check_trees(list(reversed(row)), y, axis=0)


tree_grid = tree_grid.transpose()
visible_trees = visible_trees.transpose()

# top > bottom (left > right in columns)
for x, col in enumerate(tree_grid):
    check_trees(col, x, axis=1)


# bottom > top (right > left in columns)
for x, col in enumerate(tree_grid):
    check_trees(list(reversed(col)), x, axis=1)

# turn edges into 1.0
# rows
visible_trees[0].fill(1) 
visible_trees[-1].fill(1)

# columns
visible_trees[:, 0].fill(1)
visible_trees[:, -1].fill(1)

print(sum(sum(visible_trees)))
